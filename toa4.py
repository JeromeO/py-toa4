#!/usr/bin/env python3

# Copyright (C) 2020-2020 -- Jérôme Ortais (jerome.ortais@pyromaths.org)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""
Crée un pdf au format A4 (ou A3 avec le paramètre -a3) dans lequel chaque page originale est répétée plusieurs fois par
feuille.

Crée également un fichier postscript imprimable directement depuis ma clef USB sur les photocopieurs du lycée, en
rectoverso. Le paramètre -r permet de ne faire que du recto simple.

Peut également faire du n-up (plusieurs feuilles par page) à l'aide du paramètre -s

Les pages à traiter sont données à l'aide du paramètre -p.
"""

import argparse
import re
from pathlib import Path
from subprocess import call

import PyPDF2


def eqMerdiatrice(x1, y1, x2, y2):
    """Renvoie (a, b, c) correspondant à l'équation de la droite
    ax+by+c=0"""
    milieu = ((x1 + x2) / 2, (y1 + y2) / 2)
    AB = (x2 - x1, y2 - y1)
    return AB[0], AB[1], -milieu[0] * AB[0] - milieu[1] * AB[1]


def interDroites(a, b, c, A, B, C):
    """Renvoie les coordoonéées (x,y) du point d'intersection des deux droites"""
    try:
        assert a != 0 or b != 0
    except AssertionError:
        print("La 1re équation ne représente pas une droite")
    try:
        assert A != 0 or B != 0
    except AssertionError:
        print("La 2e équation ne représente pas une droite")
    try:
        assert -A * b != -a * B
    except AssertionError:
        print("Les deux droites sont parallèles")
    if a == 0:
        y = -c / b
        x = (-C - B * y) / A
    elif A == 0:
        y = -C / B
        x = (-c - b * y) / a
    elif b == 0:
        x = -c / a
        y = (-C - A * x) / B
    elif B == 0:
        x = -C / A
        y = (-c - a * x) / b
    else:
        x = (b * C - B * c) / (a * B - b * A)
        y = (C * a - c * A) / (b * A - B * a)
    return x, y


def dim_formats(format_page, portrait=True):
    """renvoie les dimensions d'un format demandé"""
    formats = {
        "a3": {"L": 420 / 25.4 * 72, "l": 297 / 25.4 * 72},
        "a4": {"L": 297 / 25.4 * 72, "l": 210 / 25.4 * 72},
        # "a5": {"L": 210 / 25.4 * 72, "l": 148.5 / 25.4 * 72},
        # "a6": {"L": 148.5 / 25.4 * 72, "l": 105 / 25.4 * 72},
    }
    if portrait:
        return formats[format_page]["l"], formats[format_page]["L"]
    else:
        return formats[format_page]["L"], formats[format_page]["l"]


def placement(pdf_writer, pdf_reader, pages, nbpp, format_page, nup, duplex):
    l, L = dim_formats(format_page)
    if nup:
        pages_a_inserer = [pdf_reader.getPage(i) for i in pages[:min(len(pages), nbpp)]]
    else:
        pages_a_inserer = [pdf_reader.getPage(pages[0]) for _ in range(nbpp)]
    angle, largeur, longueur, nb_abs, nb_ord = orientation(pages_a_inserer[0], l, L)
    if angle != 0:
        # placement 2 - 4 // 1 - 3
        hautBas = False
        x = 0
        y = 0
    else:
        hautBas = True
        x = 0
        y = nbpp // nb_abs - 1
    for page in pages_a_inserer:
        angle, largeur, longueur, nb_abs, nb_ord = orientation(page, l, L)
        delta_x, delta_y = (l / nb_abs - largeur) / 2, (L / nb_ord - longueur) / 2
        if angle == 90 and pdf_writer.getNumPages() % 2 == 0 and duplex:
            angle = -90
        if angle == 0:
            pdf_writer.getPage(-1).mergeTranslatedPage(page, delta_x + x * l / nb_abs, delta_y + y * L / nb_ord)
        else:
            if angle % 360 == 90:
                a, b, c = eqMerdiatrice(0, 0, -delta_x + (x + 1) * l / nb_abs, delta_y + y * L / nb_ord)
                A, B, C = eqMerdiatrice(0, largeur, delta_x + x * l / nb_abs, delta_y + y * L / nb_ord)
            elif angle % 360 == 270:
                a, b, c = eqMerdiatrice(0, 0, delta_x + x * l / nb_abs, -delta_y + (y + 1) * L / nb_ord)
                A, B, C = eqMerdiatrice(longueur, 0, delta_x + x * l / nb_abs, delta_y + y * L / nb_ord)
            elif angle % 360 == 180:
                a, b, c = eqMerdiatrice(0, 0, -delta_x + (x + 1) * l / nb_abs, delta_y + y * L / nb_ord)
                A, B, C = eqMerdiatrice(0, largeur, delta_x + x * l / nb_abs, delta_y + y * L / nb_ord)
            else:
                raise ValueError("La mesure de l'angle doit être un multiple de 90°")
            tx, ty = interDroites(a, b, c, A, B, C)
            # print(tx, ty)
            pdf_writer.getPage(-1).mergeRotatedTranslatedPage(page, angle, tx, ty)
        if hautBas:
            x = (x + 1) % nb_abs
            if x == 0:
                y = y - 1
        else:
            y = (y + 1) % nb_ord
            if y == 0:
                x = x + 1

    if angle == 90 or angle == -90:
        pdf_writer.getPage(-1).rotateClockwise(90)


def orientation(page_a_inserer, largeur_insert, longueur_insert):
    largeur_totale, longueur_totale = float(page_a_inserer.mediaBox[2]), float(page_a_inserer.mediaBox[3])
    ldim = [int(round(largeur_insert / largeur_totale, 3)), int(round(longueur_insert / longueur_totale, 3)),
            int(round(largeur_insert / longueur_totale, 3)), int(round(longueur_insert / largeur_totale, 3))]
    if ldim[0] * ldim[1] > ldim[2] * ldim[3]:
        return 0, largeur_totale, longueur_totale, ldim[0], ldim[1]
    else:
        return 90, longueur_totale, largeur_totale, ldim[2], ldim[3]


def mise_en_page(page_a_inserer, dim):
    largeur_totale, longueur_totale = float(page_a_inserer.mediaBox[2]), float(page_a_inserer.mediaBox[3])
    largeur_insert, longueur_insert = dim_formats(dim)
    ldim = [int(round(largeur_insert / largeur_totale, 3)), int(round(longueur_insert / longueur_totale, 3)),
            int(round(largeur_insert / longueur_totale, 3)), int(round(longueur_insert / largeur_totale, 3))]
    if ldim[0] * ldim[1] > ldim[2] * ldim[3]:
        return ldim[0] * ldim[1]
    else:
        return ldim[2] * ldim[3]


def pages_a_traiter(no_pages):
    lp = no_pages.split(',')
    lespages = []
    for k in lp:
        enum = k.split('-')
        if len(enum) > 1:
            if int(enum[0]) < int(enum[1]):
                for j in range(int(enum[0]) - 1, int(enum[1])):
                    lespages.append(j)
            else:
                for j in range(int(enum[0]) - 1, int(enum[1]) - 2, -1):
                    lespages.append(j)
        else:
            lespages.append(int(k) - 1)
    return lespages


def main():
    parser = argparse.ArgumentParser(
        description="Crée un pdf au format a4 dans lequel chaque page originale est répétée plusieurs fois par feuille")
    parser.add_argument('file', nargs=1, help='nom du fichier à traiter')
    parser.add_argument('-o', nargs='?', help='nom du fichier en sortie', default='', const='')
    parser.add_argument('-a3', nargs='?', help='Obtenir un document A3', default='a4', const='a3')
    parser.add_argument('-p', nargs='?', help='pages à traiter : -p1-3,5,7-9', default=None, const=None)
    parser.add_argument('-s', nargs='?', help='plusieurs pages par feuille', default=False, const=True)
    parser.add_argument('-r', nargs='?', help='Postscript en recto seulement', default='-duplex', const=None)
    args = parser.parse_args()
    pdf_reader = PyPDF2.PdfFileReader(args.file[0])
    pdf_writer = PyPDF2.PdfFileWriter()

    if args.p:
        pages = pages_a_traiter(args.p)
    else:
        pages = range(pdf_reader.getNumPages())

    nbpp = mise_en_page(pdf_reader.getPage(pages[0]), args.a3)
    page_no = 0
    while page_no < len(pages):
        pdf_writer.addBlankPage(width=dim_formats(args.a3)[0], height=dim_formats(args.a3)[1])
        placement(pdf_writer, pdf_reader, pages[page_no:], nbpp, args.a3, args.s, args.r)
        if args.s:
            page_no += nbpp
        else:
            page_no += 1
    if nbpp > 1:
        if args.s:
            suffix = "-%snup" % nbpp
        else:
            suffix = "-%spp" % nbpp
    else:
        suffix = ''
    if args.p:
        if '-' in args.p or ',' in args.p:
            suffix += ('-pages%s' % args.p).replace(',', '_')
        else:
            suffix += '-page%s' % args.p

    Path('./' + args.a3.upper()).mkdir(0o755, parents=True, exist_ok=True)
    Path('./postscript').mkdir(0o755, parents=True, exist_ok=True)
    if args.o:
        output_pdf_file = Path(args.a3.upper() + '/' + args.o)
        output_ps_file = Path('postscript/' + re.sub(r'\.pdf$', '.ps', args.o))
    else:
        output_ps_file = Path('postscript/' + re.sub(r'\.pdf$', f'{suffix}.ps', args.file[0]))
        output_pdf_file = Path(args.a3.upper() + '/' + re.sub(r'\.pdf$', f'{suffix}.pdf', args.file[0]))
    with output_pdf_file.open(mode="wb") as output_file:
        pdf_writer.write(output_file)
    if args.r:
        call(["pdftops", "-level3sep", "-origpagesizes", args.r, output_pdf_file, output_ps_file])
    else:
        call(["pdftops", "-level3sep", "-origpagesizes", output_pdf_file, output_ps_file])
    print(f"Les fichiers {output_pdf_file} et {output_ps_file} ont été créés.")


if __name__ == '__main__':
    # TODO: si feuilles de tailles différentes, faire plusieurs fichiers en sortie
    # TODO: si format demandé plus petit que format de départ, erreur
    main()
