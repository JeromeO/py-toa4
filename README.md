# Introduction

Crée un fichier pdf au format A4 (ou A3 avec le paramètre `-a3`) dans lequel chaque page originale est répétée plusieurs
fois par feuille.

Crée également un fichier postscript imprimable directement depuis ma clef USB sur les photocopieurs du lycée, en
rectoverso. Le paramètre `-r` permet de ne faire que du recto simple.

Peut également faire du n-up (plusieurs feuilles par page) à l'aide du paramètre `-s`.

Les pages à traiter sont données à l'aide du paramètre `-p`.

# Exemples d'utilisation

## Sortie A4, rectoverso

Le fichier original est au format A5, portrait, sur deux pages.

L'instruction `toa4.py TSpe-chap03-activites.pdf` permet d'obtenir le document suivant, qui est prêt à être photocopié
en rectoverso, puis à massicoter pour obtenir deux fiches par feuille.

![Sans paramètre](./images/defaut.png?raw=true "Sans paramètre")

## Sortie A4, recto seulement

Le fichier original est au format A5, portrait, sur deux pages.

L'instruction `toa4.py TSpe-chap03-activites.pdf -r` permet d'obtenir le document suivant, qui est prêt à être
photocopié en recto, puis à massicoter pour obtenir deux fiches par feuille.

![Recto](./images/recto.png?raw=true "recto")

## Sortie A4, 2 feuilles par page

Le fichier original est au format A5, portrait, sur deux pages.

L'instruction `toa4.py TSpe-chap03-activites.pdf -s` permet d'obtenir le document suivant, qui est prêt à être
photocopié.

![2-nup](./images/nup.png?raw=true "2-nup")

## Sortie A4, entrée en A6

Le fichier original est au format A6, paysage, sur une page.

L'instruction `toa4.py TSpe-chap07-activites.pdf` permet d'obtenir le document suivant, qui est prêt à être photocopié,
puis à massicoter pour obtenir quatre fiches par feuille.

![A6](./images/a6toa4.png?raw=true "A6")
